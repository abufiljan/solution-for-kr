#include <stdio.h>
/* Table of convert celsius degree to
 * Fahregheit degrees where cels = -50, -45, ..., 50*/
int main() {
	float fahr, cels;
	int lower, upper, step;
	lower = -50;
	upper = 50;
	step = 5;
	
	cels = lower;
	while (cels <= upper) {
		fahr = 9.0/5.0 * cels + 32;
		printf("%3.0f%6.1f\n", cels, fahr);
		cels = cels + step;

	}



	return 0;
}
