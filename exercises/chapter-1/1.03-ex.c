#include <stdio.h>
/* Print table of temprature by Fahrengheit
 * and Celsius for fahr = 0, 20, ..., 300 */

int main() {
    float fahr, cels;
    int lower, upper, step;
    lower = 0;
    upper = 300;
    step = 20;
    printf("Table of temprature by \nFahrengheit and Celsius\n" );
	
    fahr = lower;
    while(fahr <= upper) {
	cels = (5.0/9.0) * (fahr -32.0);
	printf("%3.0f\t%6.1f\n", fahr, cels);
	fahr = fahr + step;
    }
    
}
    
